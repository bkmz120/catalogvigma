var catalogApp=angular.module('catalog', ['ngTasty','searchblock','addtocart']);



catalogApp.controller('catalogController', function($scope,$http,$location) {

	$scope.init = function(){
		$scope.catalogcats = catalogcats;
		if (catalogcat!='') 
		{
			initSearchStr = "";
			if (catalogcat!='all') $scope.search.selectCatalogcat(catalogcat);
			else $scope.search.searchAll();
		}

		$scope.search.ngTastyInit = {
			'sortBy': 'name',
			'sortOrder': 'dsc',
			
		};	

		var searchObject = $location.search();

		if (searchObject.count!=null && searchObject.page!=null)
		{
			$scope.search.ngTastyInit = {
				'sortBy': 'name',
				'sortOrder': 'dsc',
				'page':searchObject.page,
				'count':searchObject.count,				
			};	

			if (searchObject.search!=null && searchObject.search!="")
			{
				$scope.search.startSearch(searchObject.search);
			}

		}
		else
		{
			$scope.search.ngTastyInit = {
				'sortBy': 'name',
				'sortOrder': 'dsc',
				'page':1,
				'count':20,				
			};	
		}

		// if (searchObject.count!=null && searchObject.page!=null) $scope.search.ngTastyInit.count = searchObject.count;
		// else $scope.search.ngTastyInit.count = 20;

		// if (searchObject.page!=null) $scope.search.ngTastyInit.page = searchObject.page;
		// else $scope.search.ngTastyInit.page = 1;

		$scope.$on('$locationChangeSuccess',function(){
			
		});	

				
	}

	$scope.search = {
		filter: {
			searchStr:initSearchStr,
			catalogcatId:null,
		},

		selectedCatalogcatId:null,

		startSearch:function(searchStr){

			if (searchStr!="") $location.search('search', searchStr);
			else $location.search('search',null);

			$scope.search.filter = {
				searchStr:searchStr,
				catalogcatId:null,
			}
			$scope.search.selectedCatalogcatId = null; 
		},

		selectCatalogcat:function(catalogcat) {

			$scope.search.selectedCatalogcatId = catalogcat.id;
			$scope.search.filter = {
				searchStr:"",
				catalogcatId:catalogcat.id,
			}
		},

		searchAll:function(){
			$scope.search.selectedCatalogcatId = null; 
			$scope.search.filter = {
				searchStr:"",
				catalogcatId:null,
			}
		},

		getResource: function (params, paramsObj) {
			
			
			$location.search('count', paramsObj.count);
			$location.search('page', paramsObj.page);

			var url = '/catalog/productsApi/searchAjax?' + params;
			return $http.get(url).then(function (response) {
				var header = response.data.header;
				return {
					'rows': response.data.rows,
					'header': header,
					'pagination': response.data.pagination,
					'sortBy': response.data['sort-by'],
					'sortOrder': response.data['sort-order']
				}
			});
		},
	}


	

});
angular.module('countbox',[]);

angular.module('countbox').directive('countbox',['$http','$q', function($http,$q) {        
	return {
		template:   '<div class="count-box_minus" ng-click="dec()">-</div>' + 
					'<input type="text" class="count-box_inp" ng-keyup="keyup()" ng-model="countDisplay">' +
					'<div class="count-box_plus" ng-click="inc()">+</div>',

		replace: false,
		scope:{
			countboxValue:'=',
			countboxChange:'&',
		},
		restrict: 'E',
		link: 
			function (scope, element, attrs) {

				scope.countDisplay = scope.countboxValue;
				
				
				scope.callback = function()
				{
					
					if (scope.countboxChange!=null) scope.countboxChange();
							
				}

				
				scope.keyup = function()
				{
					var count = parseInt(scope.countDisplay);
					if (typeof count != 'number' || isNaN(count))
					{
						scope.countDisplay = 1;
					} 

					if (scope.countDisplay<1)
					{
						scope.countDisplay = 1;
					} 
					scope.countboxValue = scope.countDisplay;
					scope.callback();

				}

				scope.inc = function()
				{
					/*
					var inc = function()
					{
						var deferred = $q.defer();
						
						deferred.resolve('happy!');
  						// возвращаем обещание
    					return deferred.promise;
					} 					
					
					inc().then(function(){
						//scope.callback();
					});
					*/
					
					scope.countboxValue++;
					scope.countDisplay = scope.countboxValue;
					scope.callback();
				}

				scope.dec = function()
				{
					if (scope.countboxValue <= 1) return;
					scope.countboxValue--;
					scope.countDisplay = scope.countboxValue;
					scope.callback();
				}

      		},
	}
}]);
var productpageApp=angular.module('productpage', ['searchblock','addtocart']);

var counter = 0;


productpageApp.service('ContactForm', ['$http', function($http) {
    this.send = function(values)
    {
    	var url = "/catalog/formApi/ProductRequisitionAjax";

		return $http.post(url,{
			name:values.name,
			phone:values.phone,
			text:values.text,
			product:values.product,
			mode:values.mode,
		});
    }	
}]);


productpageApp.controller('productpageController', function($scope,$http,ContactForm) {

	$scope.init = function(){
		$scope.catalogcats = catalogcats;		

		$scope.product = product;		
		
	}

	$scope.search = {
		
		startSearch:function(searchStr){
			if (searchStr!=null && searchStr!="")	window.location.href = "/catalog/page/index?search="+searchStr;		
		},

		selectCatalogcat:function(catalogcat) {
			window.location.href = "/catalog/page/index?catalogcat="+catalogcat.id;	
		},

		searchAll:function(){
			window.location.href = "/catalog/page/index?catalogcat=all";
		},
	}	

	$scope.contactForm = {
		visible:false,
		mode:null,
		values:{
			name:null,
			phone:null,
			text:null,
		},
		validValues:{
			name:true,
			phone:true,
		},

		sendSuccess:false,

		/**
		 * Показать форму
		 * @param mode - режим формы. Доступные режимы: checkAvailability
		 */
		show:function(mode)
		{
			if (mode==null) {
				throw new Error('contactForm:mode is required');
			}
			this.sendSuccess = false;
			this.visible = true;
			this.mode = mode;
		},

		close:function()
		{
			this.visible = false;
			this.values={
				name:null,
				phone:null,
				text:null,
			}

			this.validValues={
				name:true,
				phone:true,
			}
		},



		send:function(){
			var valid = true;
			this.validValues.name = true;
			this.validValues.phone = true;

	    	if (this.values.name==null || this.values.name=="")
	    	{
	    		this.validValues.name = false;
	    		valid = false;
	    	}

	    	if (this.values.phone==null || this.values.phone=="")
	    	{
	    		this.validValues.phone = false;
	    		valid = false;
	    	}
	    	

	    	if (valid)
	    	{
	    		var sendValues = {
	    			name:this.values.name,
	    			phone:this.values.phone,
	    			text:this.values.text,
	    			product:$scope.product.original_name + ' ' +$scope.product.chod_display,
	    			mode:this.mode,
	    		}

	    		var self = this;
	    		ContactForm.send(sendValues).success(function (data){
	    			if (data.status)
	    			{
	    				self.sendSuccess = true;
	    			}
				});
	    	}
		},
	}

	$scope.cart = {
		added:false,
		
	}

	

	$scope.numberFormat =  function (value)
	{
        if (typeof value == 'undefined' || value==null) return 0;
		return value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
	}
});
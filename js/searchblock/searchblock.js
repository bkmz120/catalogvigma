angular.module('searchblock',[]).directive('clickAnywhereButHere', ['$document', function ($document) {
    return {
        link: function postLink(scope, element, attrs) {
            var onClick = function (event) {
               var isChild = element[0].contains(event.target);
                var isSelf = element[0] == event.target;
                var isInside = isChild || isSelf;
                if (!isInside) {
                    scope.$apply(attrs.clickAnywhereButHere)
                }
            }
            scope.$watch(attrs.isActive, function(newValue, oldValue) {
                if (newValue !== oldValue && newValue == true) {
                    $document.bind('click', onClick);
                }
                else if (newValue !== oldValue && newValue == false) {
                    $document.unbind('click', onClick);
                }
            });
        }
    };
}]);

angular.module('searchblock').directive('enterPress', function () {
    return {
        restrict:'A',
        link:function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if(event.which === 13) {
                    if (scope.selectedIndex==null) {
                        scope.visible = false;              
                        scope.selectedCallback(scope.searchStr);
                    }
                    else {
                        scope.selectResult(scope.selectedIndex);
                    }
                    scope.$apply();
                    event.preventDefault();
                }
            });
        }
   };
});

angular.module('searchblock').directive('keyNavigation', function () {
    return {
        restrict:'A',
        link:function (scope, element, attrs) {
            function unselectAll() {
                for (var i=0;i<scope.results.length;i++) {
                    scope.results[i].selected = false;
                }
            }

            element.bind("keydown keypress", function (event) {
                if (event.which === 40) {
                    if (scope.selectedIndex==null) {
                        scope.selectedIndex = 0;                        
                    }
                    else {
                        unselectAll();
                        if (scope.selectedIndex<scope.results.length-1) {
                            scope.selectedIndex ++;
                        }
                        else {
                            scope.selectedIndex=0;
                        }
                    }
                    scope.results[scope.selectedIndex].selected = true;
                    scope.$apply();
                    event.preventDefault();
                }
                if (event.which === 38) {
                    if (scope.selectedIndex==null) {
                        scope.selectedIndex = scope.results.length-1;                       
                    }
                    else {
                        unselectAll();
                        if (scope.selectedIndex>0) {
                            scope.selectedIndex--;
                            scope.results[scope.selectedIndex].selected = true;
                        }
                        else {
                            scope.selectedIndex=null;
                        }
                    }
                    
                    scope.$apply();
                    event.preventDefault();
                }
            });
        }
   };
});

angular.module('searchblock').directive('autocomlete',['$http', function($http) {        
    return {
        template:   "<div class='autocomlete' >"+
                    "   <input class='autocomlete_inp' type='text' ng-model='searchStr' enter-press key-navigation>"+
                    "   <div class='autocomlete_results' ng-show='visible' click-anywhere-but-here='close()' is-active='visible'>"+
                    "       <div class='autocomlete_result' ng-repeat='result in results track by $index' ng-click='selectResult($index)' ng-class=\"{'autocomlete_result__selected':result.selected===true}\">"+
                    "           {{result.namechod}}"+
                    "       </div>"+
                    "   </div>"+
                    "</div>",
        replace: true,
        scope:{
            searchStr:'=',
            selectedCallback:'=',
        },
        restrict: 'E',
        link: 
            function (scope, element, attrs) {
                scope.visible=false;
                scope.wasSelected = false;
                scope.firstTimeRun = true;
                
                scope.close = function() {
                    scope.visible = false;
                }

                scope.$watch('searchStr', function(newValue, oldValue) {
                    if (scope.firstTimeRun) 
                    {
                        scope.firstTimeRun = false;
                        return;
                    }
                    if (scope.searchStr=="")
                    {   console.log('selectedCallback');
                        scope.selectedCallback(scope.searchStr);
                        scope.visible=false;
                        return;
                    } 
                    if (scope.wasSelected) 
                    {
                        scope.wasSelected = false;
                        return;
                    }

                    if (angular.isUndefined(scope.searchStr))
                    {
                        scope.visible=false;
                        return; 
                    }

                    if (scope.searchStr.length<2)
                    {
                        scope.wasSelected = true;
                        scope.visible=false;
                        return; 
                    }
                    
                    var url = '/catalog/productsApi/autoCompleteAjax';
                    $http.post(url,{
                        searchStr:scope.searchStr,
                    }).success(function (data) {
                        if (angular.isArray(data) && data.length>0)
                        {
                            scope.selectedIndex = null;
                            scope.results = data;
                            scope.visible=true;
                        }
                        else
                        {
                            scope.visible=false;
                        }
                    });
                });


                scope.selectResult = function(index)
                {
                    scope.wasSelected = true;
                    scope.visible = false;
                    scope.searchStr = scope.results[index];

                    //scope.selectedCallback(scope.searchStr);
                    //console.log(scope.results[index]);
                    window.location.href = scope.results[index].url;
                }

                
            },
    }
}]);

angular.module('searchblock').directive('searchblock',['$http', function($http) {        
    return {
        template:   '<div class="search-line">'+
                    '   <div class="search_bg">'+
                    '       <autocomlete search-str="searchStr" selected-callback="search"></autocomlete>'+
                    '       <div class="search_searchBtn" ng-click="search(searchStr)">ПОИСК</div>'+ 
                    '   </div>'+
                    '</div>',

        replace: true,
        scope:{
            searchCallback:'=',
        },
        restrict: 'E',
        link: 
            function (scope, element, attrs) {
                scope.searchStr ="";

                scope.search = function(searchStr)
                {
                    scope.searchCallback(searchStr);
                    //console.log(scope.searchCallback);
                }
            },
    }
}]);
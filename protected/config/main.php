<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'My Web Application',
	'defaultController' => 'page',


	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>false,
			'loginUrl'=>array('login/login'),
		),

		//важно в куках сесии записать путь /catalog/
		//инчае кука на основном сайте не будет давать правильно аутентифицироваться
		// т.к. будет передаваться она вместо установленно yii
		'session' => array(
	    	'cookieParams' => array(
	        	'lifetime' => 86400,
	        	'path' => '/catalog/' 
	    	),
		),
		/*
		'authManager'=>array(
        	'class' => 'CPhpAuthManager',
        	'defaultRoles' => array('guest'),
    	),
    	*/

    	
		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName' =>false,
			'useStrictParsing'=>false,
			
            
			'rules'=>array(				
				'product/<url:>' => 'page/product',
				'cart'=> 'page/cart',
				'admin' => 'adminpage/import',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		
		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		*/
		// uncomment the following to use a MySQL database
		
		
		/*'db'=>array(
		 	'connectionString' => 'mysql:host=localhost;dbname=techiq_catalog',
		 	'emulatePrepare' => true,
		 	'username' => 'techiq_catalog',
		 	'password' => '5*2]9*5^',
		 	'charset' => 'utf8',
		),*/
		
        
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=techiq_catalog',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
		),
		

		
		
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'page/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

		'clientScript'=>array(
		    'packages' => array(
		    	'mainCss' => array(
		    		'baseUrl' => '/catalog/css/',
		    		'css'=> array('style.css'),
		    	),

		    	'angular' => array(
		    		'baseUrl' => '/catalog/js/libs/',
		    		'js'=> array('angular.js'),
		    	),

		    	'importPage' => array(
					// Где искать подключаемые файлы JS и CSS
					'baseUrl' => '/catalog/js/',	           
					'js'=>array('libs/ng-file-upload.min.js',
							    'import/import.js'),
					'depends'=>array('angular','mainCss'),
		        ),

		    ),
		), 
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
		'myDateFormat' => 'd-m-Y',
	),
);
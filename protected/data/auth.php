<?php
return array (
  'getCoupon' => 
  array (
    'type' => 0,
    'description' => 'получить купон',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'printCoupon' => 
  array (
    'type' => 0,
    'description' => 'напечатать купон',
    'bizRule' => NULL,
    'data' => NULL,
  ),
  'user' => 
  array (
    'type' => 2,
    'description' => '',
    'bizRule' => NULL,
    'data' => NULL,
    'children' => 
    array (
      0 => 'getCoupon',
      1 => 'printCoupon',
    ),
    'assignments' => 
    array (
      1 => 
      array (
        'bizRule' => NULL,
        'data' => NULL,
      ),
      2 => 
      array (
        'bizRule' => NULL,
        'data' => NULL,
      ),
      3 => 
      array (
        'bizRule' => NULL,
        'data' => NULL,
      ),
      4 => 
      array (
        'bizRule' => NULL,
        'data' => NULL,
      ),
      5 => 
      array (
        'bizRule' => NULL,
        'data' => NULL,
      ),
      6 => 
      array (
        'bizRule' => NULL,
        'data' => NULL,
      ),
      7 => 
      array (
        'bizRule' => NULL,
        'data' => NULL,
      ),
      8 => 
      array (
        'bizRule' => NULL,
        'data' => NULL,
      ),
    ),
  ),
);

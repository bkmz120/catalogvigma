<?php

/**
* 
*/
class ProductsManager 
{
    public static function findForPaginationByCatId($catalogcatId,$page,$count)
    {
        $connection=Yii::app()->db;
        $offset = $page*$count-$count;
        $limit = " LIMIT $offset,$count";
        $order = " ORDER BY img DESC, rating DESC";

        $rows = array();
        
        $sql="SELECT p.*
              FROM products p
              LEFT JOIN catalogcat_prods ON catalogcat_prods.product_id = p.id
              WHERE catalogcat_prods.catalogcat_id =".$catalogcatId."
              ";

        $command=$connection->createCommand($sql);
        //$command->bindParam(":catalogcat_id",$catalogcatId,PDO::PARAM_INT);
        $dataReader=$command->query();
        $totalCount = $dataReader->rowCount;

        $sql=$sql.$order.$limit;
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();
        foreach($dataReader as $row)
        {
            $rows[]=$row;
        }   

        $result = array();
        $result['totalCount'] = $totalCount;
        $result['rows'] = $rows;
        return $result; 
    }


    public static function findForPagination($searchStr,$page,$count)
    {
        $connection=Yii::app()->db;
        $offset = $page*$count-$count;
        $limit = " LIMIT $offset,$count";
        $order = " ORDER BY img DESC, rating DESC";
        

        $rows = array();
        if ($searchStr==null)
        {   $sql="SELECT * FROM products";
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            $totalCount = $dataReader->rowCount;

            $sql=$sql.$order.$limit;
            //$sql="(SELECT * FROM products ORDER BY requisitions_count DESC LI) ORDER BY img DESC".$limit;

            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            foreach($dataReader as $row)
            {
                $rows[]=$row;
            }
        }
        else
        {
            //заменим неразрывный пробел на обычный
            $searchStr = preg_replace('~\x{00a0}~siu', ' ', $searchStr);

            //найдём в строке слова похожие на ЧОД и удалим из них лишние разделители (оставив ток буквы и цифры)
            $words=preg_split("/[\s,]+/",$searchStr);
            $chodDetection = 3; // количество цифр, которое должно быть в слове что бы это был ЧОД
            $name ="";  //сформируем строки содержащие отдельно название и чод для отправки в Front-end
            $chod = "";
            for ($wC=0;$wC<count($words);$wC++)
            {
                $numericCount = 0;
                $itIsChod= false;
                for ($i=0;$i<strlen($words[$wC]);$i++)
                {
                    if (ctype_digit($words[$wC][$i]))
                    {
                        $numericCount++;
                        if ($numericCount==$chodDetection)
                        {
                            if ($chod=="")  $chod=$words[$wC];
                            else $chod = $chod.' '.$words[$wC];

                            $itIsChod = true;
                            $words[$wC] = preg_replace('/[^a-zа-яё\d]/ui','',$words[$wC]);
                            //$words[$wC] = preg_replace('/[^a-zа-яё\d]/ui','',$words[$wC]);
                            break;
                        }
                    }
                }
                if (!$itIsChod)
                {
                    if ($name=="") $name = $words[$wC];
                    else $name = $name.' '.$words[$wC];
                }
            }

            foreach ($words as $key=>$word)
            {
                $words[$key] = "+".$words[$key].'*';
            }

            $searchStr =implode(" ", $words);
            

            $sql="SELECT * FROM `input_requests` WHERE MATCH (name,chod) AGAINST ('".$searchStr."' IN BOOLEAN MODE)";
            
            $command=$connection->createCommand($sql);
            $dataReader=$command->query();
            
            $productsIds = array();

            foreach($dataReader as $row)
            {
                $newProductId = true;
                foreach ($productsIds as $productId)
                {
                    if ($row['product_id']==$productId)
                    {
                        $newProductId = false;
                        break;
                    } 
                }

                if ($newProductId)
                {
                    $productsIds[] = $row['product_id'];
                }
            }

            if (count($productsIds)!=0)
            {
                $productsIds_str = implode(",", $productsIds);

                //рассчитать общее количество
                $sql="SELECT *
                      FROM products
                      WHERE products.hidden=0 AND products.id IN (".$productsIds_str.")
                      ";

                $command=$connection->createCommand($sql);
                $dataReader=$command->query();
                $totalCount = $dataReader->rowCount;

                //взять строки для текущей пагинации
                $sql = $sql.$order.$limit;
                $command=$connection->createCommand($sql);
                $dataReader=$command->query();

                foreach($dataReader as $row)
                {
                    $rows[] = $row;
                }
            }

            
        }

        $result = array();
        $result['totalCount'] = $totalCount;
        $result['rows'] = $rows;
        return $result;
    }

    public static function findForAutoComplete($searchStr)
    {
        $connection=Yii::app()->db;

        //найдём в строке слова похожие на ЧОД и удалим из них лишние разделители (оставив ток буквы и цифры)
        $words=preg_split("/[\s,]+/",$searchStr);
        $chodDetection = 3; // количество цифр, которое должно быть в слове что бы это был ЧОД
        $name ="";  //сформируем строки содержащие отдельно название и чод для отправки в Front-end
        $chod = "";
        for ($wC=0;$wC<count($words);$wC++)
        {
            $numericCount = 0;
            $itIsChod= false;
            for ($i=0;$i<strlen($words[$wC]);$i++)
            {
                if (ctype_digit($words[$wC][$i]))
                {
                    $numericCount++;
                    if ($numericCount==$chodDetection)
                    {
                        if ($chod=="")  $chod=$words[$wC];
                        else $chod = $chod.' '.$words[$wC];

                        $itIsChod = true;
                        $words[$wC] = preg_replace('/[^a-zа-яё\d]/ui','',$words[$wC]);
                        //$words[$wC] = preg_replace('/[^a-zа-яё\d]/ui','',$words[$wC]);
                        break;
                    }
                }
            }
            if (!$itIsChod)
            {
                if ($name=="") $name = $words[$wC];
                else $name = $name.' '.$words[$wC];
            }
        }

        foreach ($words as $key=>$word)
        {
            $words[$key] = "+".$words[$key].'*';
        }


        $searchStr =implode(" ", $words);

        //$sql="SELECT * FROM `input_requests` LEFT JOIN products as p ON p.id=input_requests.product_id WHERE MATCH (name,chod) AGAINST ('".$searchStr."' IN BOOLEAN MODE) AND original_item=1 ORDER BY p.rating DESC LIMIT 0,30";
        // $sql="SELECT * FROM products as p 
        //       WHERE MATCH (p.catalog_name,p.original_chod) AGAINST ('".$searchStr."' IN BOOLEAN MODE) 
        //       AND p.hidden=0
        //       ORDER BY p.rating DESC LIMIT 0,30";
        
        $sql="SELECT DISTINCT p.id, p.catalog_name, p.catalog_chod, p.url FROM products as p 
              LEFT JOIN input_requests as ir ON ir.product_id=p.id 
              WHERE MATCH (ir.name,ir.chod) AGAINST ('".$searchStr."' IN BOOLEAN MODE) 
              AND p.hidden=0
              ORDER BY p.rating DESC LIMIT 0,10";
       

        // $sql = "SELECT * FROM products
        //      LEFT JOIN input_requests as ir ON products.id=ir.product_id 
        //      WHERE MATCH (ir.name,ir.chod) AGAINST ('".$searchStr."' IN BOOLEAN MODE) AND ir.original_item=1 
        //      ORDER BY p.rating DESC LIMIT 0,30
        // ";
        $command=$connection->createCommand($sql);
        $dataReader=$command->query();

        $results = array();
        foreach ($dataReader as $row)
        {
            $namechod=$row['catalog_name'].' '.$row['catalog_chod'];
            $url = "/catalog/product/".$row['url'];
            // $notFound = true;
            // foreach ($results as $result)
            // {
            //     if (strcmp($result['namechod'], $namechod)==0)
            //     {
            //         $notFound = false;
            //         break;
            //     }
            // }
            // if ($notFound) {
            //     $results[] = array(
            //         'namechod'=>$namechod,
            //         'url'=> $url
            //     );
            // }
            $results[] = array(
                'namechod'=>$namechod,
                'url'=> $url
            );
            if (count($results)>=10) break;
        }

        return $results;
    }


    /**
     * Выдает данные для отображения на странице товара
     * 
     */ 
    public static function getInfoByUrl($url)
    {
        /*
        $criteria = new CDbCriteria;
        $criteria->condition = "url=:url";
        $criteria->params = array(":url"=>$url);

        $product = Products::model()->find($criteria);
        return $product->attributes; 
        */
        $connection=Yii::app()->db;

        //инфа о товаре

        $sql="SELECT * FROM `products` p  WHERE p.url =:url";
        $command=$connection->createCommand($sql);
        $command->bindParam(":url",$url,PDO::PARAM_STR);
        $dataReader=$command->queryAll();

        if (count($dataReader)==0) throw new CHttpException(404,'Товар не найден'); 
        

        $product = $dataReader[0];

        if ($product['weight']!=null) $product['weight'] = round($product['weight'],3);
        if ($product['size']!=null)
        {
            $sizeDisplay = "";
            $split = explode(';',str_replace(',','.',$product['size']));
            
            
            for ($i=0;$i<3;$i++)
            {
                
                if (is_numeric($split[$i]))
                {
                    if ($i!=0) $sizeDisplay = $sizeDisplay." * ";
                    $sizeDisplay = $sizeDisplay.$split[$i];
                }
                else
                {
                    $sizeDisplay = null;
                    break;
                }
            }
            $product['size'] = $sizeDisplay;
        }

        //категории товара
        $sql = "SELECT c.name_case FROM `catalogcat_prods` cp
                LEFT JOIN `catalogcats` c ON cp.catalogcat_id = c.id
                WHERE cp.product_id =:id ";
        $command=$connection->createCommand($sql);

        $command->bindParam(":id",$product['id'],PDO::PARAM_INT);
        $dataReader= $command->query();

        $product['catalogcats'] = array();

        foreach ($dataReader as $row)
        {
            $product['catalogcats'][] = $row['name_case'];
        }

        $product['catalogcats_str'] = implode(',', $product['catalogcats']);
        
        //группы товара

        $sql = "SELECT g.group_num FROM `group_prods` gp
                LEFT JOIN `groups` g ON g.id=gp.group_id
                WHERE gp.product_id=:product_id
                ";
        $command=$connection->createCommand($sql);

        $command->bindParam(":product_id",$product['id'],PDO::PARAM_INT);

        $dataReader = $command->query();        
        $product['groupsNums'] = array();

        foreach ($dataReader as $row)
        {
            $product['groupsNums'][] = $row['group_num'];
        }

        if (count($product['groupsNums'])>0)
        {
            $product['groupsNums_str'] = implode(',', $product['groupsNums']);
            if (count($product['groupsNums'])==1) $product['groupsNums_str'] = "(группа:".$product['groupsNums_str'].")";
            else $product['groupsNums_str'] = "(группы:".$product['groupsNums_str'].")";
        }


        //остатки

        $sql = "SELECT city.name as city_name,city.id as city_id,  stock.val1, stock.val2, stock.val3 FROM stock_cities city
                LEFT JOIN stocks stock ON stock.product_id =:product_id  AND stock.stockcity_id = city.id
                WHERE 1
        ";
        $command=$connection->createCommand($sql);
        $command->bindParam(":product_id",$product['id'],PDO::PARAM_INT);
        $rows = $command->queryAll();

        //статус наличия в городах: inStock | inChelyabinsk | needClarify
        $product['stocks'] = array();       

        //Челябинск
        $stockChe = array();
        $stockChe['name'] = 'Челябинск';
        $stockChe['status'] = "needClarify";
        foreach ($rows as $row) {
            if ($row['city_id']==1 || $row['city_id']==4 || $row['city_id']==5) {
                if ($row['val1']>0 || $row['val2']>0 || $row['val3']>0) {
                    $stockChe['status'] = "inStock";
                }               
            }
        }
        $product['stocks'][] = $stockChe;

        //Екатеринбург
        $stockEkb = array();
        $stockEkb['name'] = 'Екатеринбург';
        $stockEkb['status'] = "needClarify";
        foreach ($rows as $row) {
            if ($row['city_id']==3) {
                if ($row['val1']>0 || $row['val2']>0 || $row['val3']>0) {
                    $stockEkb['status'] = "inStock";
                }                   
            }
        }
        if ($stockEkb['status'] == "needClarify" && $stockChe['status']== "inStock") {
            $stockEkb['status'] = "inChelyabinsk";
        }
        $product['stocks'][] = $stockEkb;

        //Казань
        $stockKaz = array();
        $stockKaz['name'] = 'Казань';
        $stockKaz['status'] = "needClarify";
        foreach ($rows as $row) {
            if ($row['city_id']==2) {
                if ($row['val1']>0 || $row['val2']>0 || $row['val3']>0) {
                    $stockKaz['status'] = "inStock";
                }               
            }
        }
        if ($stockKaz['status'] == "needClarify" && $stockChe['status']== "inStock") {
            $stockKaz['status'] = "inChelyabinsk";
        }
        $product['stocks'][] = $stockKaz;

        //Уфа
        //Уфу ищем по-имени т.к. id для неё не был задан на момент написания
        $stockKaz = array();
        $stockKaz['name'] = 'Уфа';
        $stockKaz['status'] = "needClarify";
        foreach ($rows as $row) {
            $city_name = trim(strtolower($row['city_name']));
            if ($city_name =='Уфа') {
                if ($row['val1']>0 || $row['val2']>0 || $row['val3']>0) {
                    $stockKaz['status'] = "inStock";
                }               
            }
        }
        if ($stockKaz['status'] == "needClarify" && $stockChe['status']== "inStock") {
            $stockKaz['status'] = "inChelyabinsk";
        }
        $product['stocks'][] = $stockKaz;
        

        return $product;
    }
    
}
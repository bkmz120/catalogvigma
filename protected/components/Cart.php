<?php

/**
*  Функционал для работы с корзиной
*/
class Cart
{
	const CART_STATUS_NEW = "new";
	const CART_STATUS_CHECKOUT = "checkout";

	private $userCart;


	function __construct()
	{
		if ($_COOKIE['cartIdent']!=null)
		{
			$cartIdent = json_decode($_COOKIE['cartIdent'],true);
		}
		else
		{
			$cartIdent = null;
		}

		if ($cartIdent!=null)
		{
			$userCart = UserCart::model()->findByPk($cartIdent['id']);
			if ($userCart!=null && $userCart->status==self::CART_STATUS_NEW) {
				$this->userCart = $userCart;				
			}
			else {
				$this->createNewCart();
			}
			
		}
		else
		{
			$this->createNewCart();
		}
	}

	/** 
	 * Созадть новую запись в таблице userCart и сохранить её id в куках
	 * 
	 */ 
	private function createNewCart()
	{
		$userCart = new UserCart();
		$userCart->created = new CDbExpression('NOW()');
		$userCart->status = "new";
		$userCart->save();
		$this->userCart = $userCart;

		$cartIdent = array('id'=>$userCart->id);

		Yii::app()->request->cookies['cartIdent'] = new CHttpCookie('cartIdent', json_encode($cartIdent),array('expire'=>time()+60*60*24*30));
	}

	/**
	 * Добавить товар в корзину
	 * @param int $product_id
	 * @param int $count
	 * @param 
	 * @return bool result
	 */ 
	public function addProduct($product_id,$count,$price_catalog)
	{
		if ($this->userCart->products!=null)
		{
			$products =  json_decode($this->userCart->products,true);
		}
		else
		{
			$products = array();
		}

		$itIsNewProduct = true;
		foreach ($products as $i=>$product)
		{
			if ($product['id']==$product_id)
			{
				$products[$i]['count'] += $count;
				$itIsNewProduct = false;
				break;
			}
		}

		if ($itIsNewProduct)
		{
			$products[] = array('id'=>$product_id,'count'=>$count,'price_catalog'=>$price_catalog);
		}


		$this->userCart->products = json_encode($products);
		$res=$this->userCart->update();		

		return $res;
	}

	/**
	 * Удалить товар из корзины
	 * @param int $product_id
	 * @return bool result 	 * 
	 */ 
	public function deleteProduct($product_id)
	{
		if ($this->userCart->products==null) return false;

		$products =  json_decode($this->userCart->products,true);

		for ($i=0;$i<count($products);$i++)
		{
			if ($products[$i]['id']==$product_id)
			{
				array_splice($products,$i, 1);
				break;
			}
		}

		$this->userCart->products = json_encode($products);
		$res=$this->userCart->update();		

		return $res;		
	}

	/**
	 * Удалить все товары
	 * 
	 * @return bool result 	 * 
	 */ 
	public function deleteAllProducts()
	{
		if ($this->userCart->products==null) return false;

		$products = array();
		

		$this->userCart->products = json_encode($products);
		$res=$this->userCart->update();		

		return $res;		
	}	

	/**
	 * Изменить количество товара в корзине
	 * @param int $product_id
	 * @param int $count
	 * @return bool result
	 */
	public function changeCount($product_id,$count)
	{
		if ($this->userCart->products==null) return false;

		$products =  json_decode($this->userCart->products,true);

		$productNotFound = true;
		for ($i=0;$i<count($products);$i++)
		{
			if ($products[$i]['id']==$product_id)
			{
				$products[$i]['count'] = $count;
				$productNotFound = false;
				break;
			}
		}

		if ($productNotFound) return false;

		$this->userCart->products = json_encode($products);
		$res=$this->userCart->update();		

		return $res;
	}


	/**
	 * Получить массив товаров в корзине
	 * @return array $cartProducts
	 */ 

	public function getProducts()
	{
		if ($this->userCart->products==null) return array();

		$cartProductsIdCount = json_decode($this->userCart->products,true);

		if (count($cartProductsIdCount)==0) return array();

		$productsIds = array();
	
		foreach ($cartProductsIdCount as $cartProductIdCount)
		{
			$productsIds[] = $cartProductIdCount['id'];
		}

		$productIdsStr = implode(',', $productsIds);

		
		
		$sql = "SELECT * FROM products WHERE id IN (".$productIdsStr.") ";
		$connection=Yii::app()->db;
		$command=$connection->createCommand($sql);
		
		$productsRows = $command->queryAll();



		$cartProducts = array();

		foreach ($productsRows as $product)
		{
			for ($i=0;$i<count($cartProductsIdCount);$i++)
			{
				if ($cartProductsIdCount[$i]['id']==$product['id'])
				{
					$cartProducts[] = array('product'=>$product, 
											'count'=>$cartProductsIdCount[$i]['count'],
											'price'=>$product['price_catalog']*$cartProductsIdCount[$i]['count'],
											);
					break;
				}
			}
		}

		return $cartProducts;
	}

	/**
	 * Получить список товаров в виде форматированого текста
	 * @return array $cartProducts
	 */ 
	public function getProductsAsText($newLine="\r\n")
	{
		$cartProducts = $this->getProducts();

		$text = "";

		foreach ($cartProducts as $i=>$cartProduct)
		{
			$text.=($i+1).'. '.$cartProduct['count'].' шт. '.$cartProduct['product']['catalog_name'].' '.$cartProduct['product']['catalog_chod'].$newLine;
		}

		return $text;
	}

	/**
	 * Смена статуса корзины на "отправлен" (checkout)
	 * @param string $name
	 * @param string $phone
	 * @param string $text
	 * @return bool status
	 */
	public function checkout($name,$phone,$text) {
		$this->userCart->name = $name;
		$this->userCart->phone = $phone;
		$this->userCart->text = $text;
		$this->userCart->status = "checkout";
		return $this->userCart->update();	
	}
}

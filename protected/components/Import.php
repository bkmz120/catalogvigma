<?php
/**
 * Реализует функционал импорта базы данных
 */
class Import {
	
	/**
	 * Разорхивирует архив в папку <путь до директории загрузки архивов>/current 
	 * если current уже существовала, то очищает её
	 * 
	 * @param string $filePath путь до архива
	 */

	public static function unZip($filePath)
	{
		$ds=DIRECTORY_SEPARATOR;
		$unzipDir = UploadFile::getUploadDirPath("zip")."current".$ds;

		//удалиь результаты прошлого разархивирования, если они есть
		if (is_dir($unzipDir))
		{
			self::deleteDir($unzipDir);
		}

		if (!mkdir($unzipDir))
		{
			throw new Exception("can't create $unzipDir");
		}
	
		$zip = new ZipArchive;
		$res = $zip->open($filePath);
		if ($res === TRUE) 
		{
			$zip->extractTo($unzipDir);
			$zip->close();
		} 
		else 
		{
			throw new Exception("can't open $filePath");
		}
		
	}

	/**
	 * Рекурсивно удает директорию
	 * 
	 * @param string $dirPath - путь до директории
	 */ 
	private static function deleteDir($dirPath) {
	    if (! is_dir($dirPath)) {
	        throw new InvalidArgumentException("$dirPath must be a directory");
	    }
	    if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
	        $dirPath .= '/';
	    }
	    $files = glob($dirPath . '*', GLOB_MARK);
	    foreach ($files as $file) {
	        if (is_dir($file)) {
	            self::deleteDir($file);
	        } else {
	            unlink($file);
	        }
	    }
	    rmdir($dirPath);
	}

	/*
	 * Очисить таблицы в БД перед импортом дампа 
	 */
	public static function dropTables()
	{
		$connection=Yii::app()->db;	
		$sql = "DROP TABLE IF EXISTS `input_requests`;
				DROP TABLE IF EXISTS `catalogcats`;
				DROP TABLE IF EXISTS `catalogcat_prods`;
				DROP TABLE IF EXISTS `groups`;
				DROP TABLE IF EXISTS `group_prods`;
				DROP TABLE IF EXISTS `stocks`;
				DROP TABLE IF EXISTS `products`;
			   ";
		//$sql = "DROP TABLE IF EXISTS `products`;";
		$command=$connection->createCommand($sql);
		$res=$command->execute();	
	}



	/**
	 * Импорт дампа базы данных, удаление лишних колонок из таблиц product и input_reuests
	 *  
	 */ 

	public static function importDb()
	{
		self::dropTables();
		$ds=DIRECTORY_SEPARATOR;
		$filePath =  UploadFile::getUploadDirPath("zip")."current".$ds."dump.sql";

		if (!file_exists($filePath))
		{
			throw new Exception("Dump file not found: $filePath");
			
		}

		$connection=Yii::app()->db;		
				
		$templine = '';
		// Read in entire file
		$lines = file($filePath);
		// Loop through each line
		foreach ($lines as $line)
		{
			// Skip it if it's a comment
			if (substr($line, 0, 2) == '--' || $line == '')
			    continue;

			// Add this line to the current segment
			$templine .= $line;
			// If it has a semicolon at the end, it's the end of the query
			if (substr(trim($line), -1, 1) == ';')
			{
				//echo $templine;
			    // Perform the query
			    //mysql_query($templine) or print('Error performing query \'<strong>' . $templine . '\': ' . mysql_error() . '<br /><br />');
			    // Reset temp variable to empty
			    $sql = $templine;		

			    $command=$connection->createCommand($sql);
			    $res=$command->execute();					

			    $templine = '';
			}
		}

		$columnsInputRequsetForDrop = array('comment','provider_id','provprod_id','chod_utochn','chod_chtz','name_chtz','pokupka','istochnik','price_in','use_default');

		$command = Yii::app()->db->createCommand();

		foreach ($columnsInputRequsetForDrop as $column)
		{
			$res=$command->dropColumn('input_requests',$column);
			
		}

		$columnsProductForDrop = array('comment','tovar_type','other_info','defaultprovider_id','defaultprovprod_id','price_chtz','price_out','parts_id','partof_id','use_deafult');

		foreach ($columnsProductForDrop as $column)
		{
			$res=$command->dropColumn('products',$column);
		}

		//создать полнотектовый индекс в таблице input_requests по полям name,chod
		$sql = "ALTER TABLE input_requests
				ADD FULLTEXT INDEX 'namechod' ('name' ASC,'chod' ASC);";
		$command=$connection->createCommand($sql);
		//$res=$command->execute();
		
	}

	/**
	 * Рекурсивное копирование директории
	 * 
	 * @param string $source
	 * @param string $dest
	 * 
	 */ 
	public static function copyr($source, $dest)
	{
	    // Check for symlinks
	    if (is_link($source)) {
	        return symlink(readlink($source), $dest);
	    }
	    
	    // Simple copy for a file
	    if (is_file($source)) {
	        return copy($source, $dest);
	    }

	    // Make destination directory
	    if (!is_dir($dest)) {
	        mkdir($dest);
	    }

	    // Loop through the folder
	    $dir = dir($source);
	    while (false !== $entry = $dir->read()) {
	        // Skip pointers
	        if ($entry == '.' || $entry == '..') {
	            continue;
	        }

	        // Deep copy directories
	        self::copyr("$source/$entry", "$dest/$entry");
	    }

	    // Clean up
	    $dir->close();
	    return true;
	}

	
	public static function copyPhotos()
	{
		$ds=DIRECTORY_SEPARATOR;
		$newPhotosPath =  UploadFile::getUploadDirPath("zip")."current".$ds."photos".$ds;
		if (!is_dir($newPhotosPath))
		{
			throw new Exception("Photos dir not found: $newPhotosPath");			
		}

		$oldPhotosPath = Yii::app()->basePath.$ds."..".$ds."photos".$ds;
		//удалить старые фотки
		$files = glob($oldPhotosPath . '*', GLOB_MARK);
	    foreach ($files as $file) {
	        if (is_file($file))
	        {
	            unlink($file);
	        }
	    }

	    self::copyr($newPhotosPath,$oldPhotosPath);

	}


}
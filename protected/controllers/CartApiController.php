<?php

class CartApiController extends CController
{
	public function actionAdd()
	{
		$data = json_decode(file_get_contents('php://input'));

		$response = new AjaxResponse;		

		try
		{
			$product = Products::model()->findByPk($data->product_id);
			if ($product==null) {
				throw new Exception("Product not found", 1);				
			}
			$cart = new Cart;
			$res=$cart->addProduct($data->product_id,$data->count,$product->price_catalog);
			if (!$res) $response->setError("error on update");
		
		}
		catch (Exception $e)
		{
			$response->setError($e->getMessage());
		}
		
		$response->send();		
	}

	public function actionDelete()
	{
		$data = json_decode(file_get_contents('php://input'));

		$response = new AjaxResponse;		

		try
		{
			$cart = new Cart;
			$res=$cart->deleteProduct($data->product_id);
			if (!$res) $response->setError("error on update");
		
		}
		catch (Exception $e)
		{
			$response->setError($e->getMessage());
		}
		
		$response->send();	
	}

	public function actionClearCart()
	{
		$response = new AjaxResponse;		

		try
		{
			$cart = new Cart;
			$res=$cart->deleteAllProducts();
			if (!$res) $response->setError("error on update");
		
		}
		catch (Exception $e)
		{
			$response->setError($e->getMessage());
		}
		
		$response->send();	
	}


	public function actionChangeCount()
	{
		$data = json_decode(file_get_contents('php://input'));

		$response = new AjaxResponse;		

		try
		{
			$cart = new Cart;
			$res=$cart->changeCount($data->product_id,$data->count);
			if (!$res) $response->setError("error on update");
		
		}
		catch (Exception $e)
		{
			$response->setError($e->getMessage());
		}
		
		$response->send();		
	}


}


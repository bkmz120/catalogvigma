<?php

class PageController extends CController
{
	public $layout='/layouts/catalogLayout';
	
	public function actionIndex($search=null,$catalogcat=null)
	{
		$catalogcats = CatalogcatsManager::getAllInfos();


		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		//$cs->registerCssFile($baseUrl.'/css/main.css?2');
		$cs->registerScriptFile($baseUrl.'/js/libs/jquery.js');
		$cs->registerScriptFile($baseUrl.'/js/libs/angular.js');
		$cs->registerScriptFile($baseUrl.'/js/libs/ng-tasty.js');

		$cs->registerScriptFile($baseUrl.'/js/searchblock/searchblock.js');
		$cs->registerScriptFile($baseUrl.'/js/countbox/countbox.js');
		$cs->registerScriptFile($baseUrl.'/js/addtocart/addtocart.js');
		$cs->registerScriptFile($baseUrl.'/js/catalog/catalog.js');

		$this->render('index',array('catalogcats'=>$catalogcats,'initSearchStr'=>$search,'catalogcat'=>$catalogcat));
	}

	public function actionProduct($url)
	{
		$catalogcats = CatalogcatsManager::getAllInfos();

		$productInfo = ProductsManager::getInfoByUrl($url);

		
		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		//$cs->registerCssFile($baseUrl.'/css/main.css?2');
		$cs->registerScriptFile($baseUrl.'/js/libs/jquery.js');
		$cs->registerScriptFile($baseUrl.'/js/libs/decimal.min.js');
		$cs->registerScriptFile($baseUrl.'/js/libs/angular.js');
		$cs->registerScriptFile($baseUrl.'/js/libs/ng-tasty.js');

		$cs->registerScriptFile($baseUrl.'/js/searchblock/searchblock.js');
		$cs->registerScriptFile($baseUrl.'/js/countbox/countbox.js');
		$cs->registerScriptFile($baseUrl.'/js/addtocart/addtocart.js');
		$cs->registerScriptFile($baseUrl.'/js/productpage/productpage.js');

		$this->render('productpage',array('product'=>$productInfo,'catalogcats'=>$catalogcats));
	}

	public function actionCart()
	{
		$cart = new Cart();
		$cartProducts = $cart->getProducts();

		$baseUrl = Yii::app()->baseUrl; 
		$cs = Yii::app()->getClientScript();
		$cs->registerScriptFile($baseUrl.'/js/libs/angular.js');

		$cs->registerScriptFile($baseUrl.'/js/countbox/countbox.js');
		$cs->registerScriptFile($baseUrl.'/js/cartpage/cartpage.js');

		$this->render('cartpage',array('cartProducts'=>$cartProducts));
	}	

	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			$this->render('error', $error);
		}
	}
}
<?php

class ImportApiController extends CController
{
	public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
	public function accessRules()
    {
        return array(
            
             array('allow', // allow authenticated users to perform any action
	            'users'=>array('@'),
		        ),
	        array('deny',  // deny all users
	            'users'=>array('*'),
	        	),
        );
    }

	
	public $layout='/layouts/catalogLayout';


	public function actionUnzipAjax()
	{
		$data = json_decode(file_get_contents('php://input')); 

		$filePath =  UploadFile::getPathByName($data->fileName,'zip');
		
		$response = array('status'=>true);

		try
		{
			Import::unZip($filePath);
		}
		catch(Exception $ex)
		{
			$response['status'] = false;
			$response['message'] = $ex->getMessage();
		}
		
		echo json_encode($response);
	}
	
	
	public function actionImportDbAjax()
	{
		$data = json_decode(file_get_contents('php://input')); 

		
		$response = array('status'=>true);

		try
		{
			Import::importDb();
		}
		catch(Exception $ex)
		{
			$response['status'] = false;
			$response['message'] = $ex->getMessage();
		}
		
		echo json_encode($response);		
	}

	public function actionCopyPhotosAjax()
	{
		$data = json_decode(file_get_contents('php://input')); 

		
		$response = array('status'=>true);

		try
		{
			Import::copyPhotos();
		}
		catch(Exception $ex)
		{
			$response['status'] = false;
			$response['message'] = $ex->getMessage();
		}
		
		echo json_encode($response);		
	}

	
	/*
	public function actionTranslitAjax()
	{
		$response = array('status'=>true);

		try
		{
			$result = Import::translitUrl();
			$response['result'] = $result;
		}
		catch(Exception $ex)
		{
			$response['status'] = false;
			$response['message'] = $ex->getMessage();
		}
		
		echo json_encode($response);	
	}
	*/


	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			$this->render('error', $error);
		}
	}
}
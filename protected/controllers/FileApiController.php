<?php
class FileApiController extends CController
{
	public function filters()
    {
        return array(
            'accessControl',
        );
    }
    
    public function accessRules()
    {
        return array(
            
             array('allow', // allow authenticated users to perform any action
                'users'=>array('@'),
                ),
            array('deny',  // deny all users
                'users'=>array('*'),
                ),
        );
    }


    public function actionUploadFileAjax()
    {
        $uploadType = $_POST['uploadType'];

        $res= UploadFile::upload($_FILES["file"],$uploadType);
        echo $_FILES["file"]["name"];        
    }

}
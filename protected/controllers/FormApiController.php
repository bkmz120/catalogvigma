<?php

class FormApiController extends CController
{
	public function actionProductRequisitionAjax()
	{
		$data = json_decode(file_get_contents('php://input'));
		
    
		$to = "info@vigma.ru,bkmz120@gmail.com";

		$subj = "";
		$text = "";
		switch ($data->mode) {
			case 'checkAvailability':
				$subj = "Уточнить наличие товара";
				$text ='Получена заявка на уточнение наличия товара:'. "\r\n";
				break;			
		}		
		
		$text.='Имя: '.$data->name. "\r\n" .
		       'Телефон: '.$data->phone. "\r\n"; 

		if ($data->text!=null && $data->text!="") 
		{
		    $text = $text.'Комментарий: '.$data->text. "\r\n";
		} 

		$text.= "Товар: ".$data->product;

		$response = array('status'=>true);		

		$res=mail ($to,$subj,$text);
    	if (!$res)
	    {
	    	$response['status'] = false;
	    	$response['message'] = "error while sending";
	    }

		echo json_encode($response);
	}


	public function actionCartRequisitionAjax() 
	{
		$data = json_decode(file_get_contents('php://input'));

		$cart = new Cart();
		$cart->checkout($data->name,$data->phone,$data->text);
		$cartProducts = $cart->getProductsAsText();

		$to = "info@vigma.ru,bkmz120@gmail.com";
		$subj = "Заявка на покупку из корзины каталога";
		$text ='Получена заявка на покупку из каталога:'. "\r\n\r\n" .
		       'Имя: '.$data->name. "\r\n\r\n" .
		       'Телефон: '.$data->phone. "\r\n\r\n"; 

		if ($data->text!=null && $data->text!="") 
		{
		    $text = $text."Комментарий:\r\n".$data->text. "\r\n\r\n";
		} 

		$text.= "Товары:\r\n".$cartProducts;

		$res=mail ($to,$subj,$text);

		$response = new AjaxResponse;
		$response->setDataItem('text',$text);
		$response->send();
	}
}
<?php

class ProductsApiController extends CController
{
	public function actionSearchAjax($searchStr=null,$catalogcatId = null,$page=null,$count=null)
	{
		if ($catalogcatId!=null) $searchResults = ProductsManager::findForPaginationByCatId($catalogcatId,$page,$count);
		else $searchResults = ProductsManager::findForPagination($searchStr,$page,$count);

		$result = array();
		$result['rows'] = $searchResults['rows'];
		$result['header'] = array();
		$result['pagination'] = array("count"=>$count,
									  "page"=>$page,
									  "pages"=>($searchResults['totalCount']-$searchResults['totalCount']%$count)/$count,
									  "size"=>$searchResults['totalCount'],
									 );
		$result['sortBy'] = null;
		$result['sortOrder'] = null;

		echo json_encode($result);
	}

	public function actionAutoCompleteAjax()
	{
        $data = json_decode(file_get_contents('php://input'));
		$searchResults = ProductsManager::findForAutoComplete($data->searchStr);

		//var_dump(JSON_UNESCAPED_UNICODE);

		echo json_encode($searchResults);
	}
}
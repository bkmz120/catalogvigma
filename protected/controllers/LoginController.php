<?php

class LoginController extends CController
{
	public $layout='/layouts/catalogLayout';

	public function actionLogin()
	{
		$error;
		if (isset($_POST))
		{
			if (isset($_POST['username']) && isset($_POST['password']))

			{

				$identity=new UserIdentity($_POST['username'],$_POST['password']);
				if($identity->authenticate())
				{
				    Yii::app()->user->login($identity);
				   	//var_dump(session_id());
				    Yii::app()->request->redirect($this->createUrl("adminpage/import"));
				}
				else
				    $error = 'login or passwor incorrect!';
			}
		}

		$this->render('login',array('error'=>$error));
	}

	public function actionLogout()
    {
    	Yii::app()->user->logout();
    	Yii::app()->request->redirect($this->createUrl("page/index"));
    }

}
<?php

/**
 * This is the model class for table "products".
 *
 * The followings are the available columns in table 'products':
 * @property integer $id
 * @property string $original_name
 * @property string $original_chod
 * @property string $chod_display
 * @property string $catalog_name
 * @property string $catalog_chod
 * @property string $product_num
 * @property string $price_catalog
 * @property integer $catalogcat_id
 * @property integer $hidden
 * @property string $size
 * @property double $weight
 * @property string $img
 */
class Products extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'products';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('catalogcat_id, hidden', 'numerical', 'integerOnly'=>true),
			array('weight', 'numerical'),
			array('original_name, original_chod, product_num, size', 'length', 'max'=>256),
			array('chod_display, img', 'length', 'max'=>255),
			array('price_catalog', 'length', 'max'=>12),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, original_name, original_chod, chod_display, catalog_name, catalog_chod, product_num, price_catalog, catalogcat_id, hidden, size, weight, img', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'input_requests' => array(self::HAS_MANY, 'InputRequests', 'product_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'original_name' => 'Original Name',
			'original_chod' => 'Original Chod',
			'chod_display' => 'Chod Display',
			'catalog_name' => 'Catalog Name',
			'catalog_chod' => 'Catalog Chod',
			'product_num' => 'Product Num',
			'price_catalog' => 'Price Catalog',
			'catalogcat_id' => 'Catalogcat',
			'hidden' => 'Hidden',
			'size' => 'Size',
			'weight' => 'Weight',
			'img' => 'Img',
		);
	}

	


	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);

		$criteria->compare('original_name',$this->original_name,true);

		$criteria->compare('original_chod',$this->original_chod,true);

		$criteria->compare('chod_display',$this->chod_display,true);

		$criteria->compare('product_num',$this->product_num,true);

		$criteria->compare('price_catalog',$this->price_catalog,true);

		$criteria->compare('catalogcat_id',$this->catalogcat_id);

		$criteria->compare('hidden',$this->hidden);

		$criteria->compare('size',$this->size,true);

		$criteria->compare('weight',$this->weight);

		$criteria->compare('img',$this->img,true);

		return new CActiveDataProvider('Products', array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * @return Products the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
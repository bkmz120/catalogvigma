<?php
$url=$this->createUrl("login/login");


?>

<style type="text/css">

.login-form-wrapper {
	padding: 20px;
	border: 1px solid #ccc;
	border-radius: 5px;
	margin:0 auto;
	margin-top: 25px;
	margin-bottom: 40px;
	width:300px;
}

.login-form_message {
	color: red;
	font-weight: bold;
}

.login-form_inp {
	display: block;
    width: 273px;
    height: 34px;
    padding: 0px 12px;
    font-size: 14px;
    line-height: 40px;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    margin-bottom: 10px;
}

</style>

<div class="login-form-wrapper">
<div class="login-form_message"> <?if ($error!=null) echo $error;?> </div>
<form action='<?=$url ?>' method="post">
	<p>
		<input class="login-form_inp" name='username' type='text' placeholder="Логин" autofocus>
	</p>
	<p>
		<input class="login-form_inp" name='password' type='password' placeholder="Пароль">
	</p>
	<p><input id="login" class="btn btn-info " type="submit" value='Вход'></p>
</form>
</div>
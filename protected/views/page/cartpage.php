<div ng-app="CartPageApp">
	<div ng-controller="CartController" ng-init="init()"  ng-cloak>
		<div class="cart-editor" id="cart-editor">
			<div class="cart-title">Корзина</div>
			<div class="cart-products" ng-class="{'cart-products__empty':cart.cartProducts.length==0}">
				<div class="cart-products_item" ng-repeat="cartProduct in cart.cartProducts">
					<div class="cart-products_item-photo">
						<img class="cart-products_item-photo-img" src="/catalog{{cartProduct.product.img}}">
					</div>
				
					<div class="cart-products_item-chodname">
						<div class="cart-products_item-chodname-inner">
							<div class="cart-products_item-chodname-text">{{cartProduct.product.catalog_name}}<br>{{cartProduct.product.catalog_chod}}</div>
						</div>
					</div>
					<div class="cart-products_item-count">
						<countbox class="count-box" countbox-value="cartProduct.count" countbox-change="cart.onChangeCartProductCount(cartProduct)" ></countbox>
					</div>

					<div class="cart-products_item-price">{{cartProduct.price.toFixed(2)}} руб</div>
					
					<div class="cart-products_item-delete">
						<img class="cart-products_item-delete-img" 
							 src="/catalog/img/delete.png" 
							 ng-click="cart.deleteCartProduct($index)"
							 title="Удалить из корзины" 
							 alt="Удалить из корзины"
						>
					</div>
				</div>

				<div class="cart-products_clearAllBtn" 
					 ng-show="cart.cartProducts.length>1" 
					 ng-click="cart.onClearCart()"
				>Очистить корзину</div>

				<div class="cart-products_emptyCartLabel" ng-show="cart.cartProducts.length==0">Корзина пуста</div>
			</div>

			<div class="cart-requisition cart-requisition__init" id="reqForm">
				<div class="cart-req_sumTotal">
					<span class="cart-req_sumTotal-label">Сумма заявки: </span>
					<span class="cart-req_sumTotal-val">{{cart.totalPrice.toFixed(2)}} р.</span>
				</div>

				
				<div class="cart-req_title">Оформление заявки</div>

				<div class="cart-req_label">Ваше имя:</div>
				<div class="cart-req_required" ng-class="{'cart-req_required__visible':!reqForm.validValues.name}">обязательное поле</div>
				<input type="text" class="cart-req_inp" ng-model="reqForm.values.name" ng-class="{'cart-req_inp__error':!reqForm.validValues.name}">

				<div class="cart-req_label">Телефон:</div>
				<div class="cart-req_required" ng-class="{'cart-req_required__visible':!reqForm.validValues.phone}">обязательное поле</div>
				<input type="text" class="cart-req_inp" ng-model="reqForm.values.phone" ng-class="{'cart-req_inp__error':!reqForm.validValues.phone}">

				<div class="cart-req_label">Допонительная информация:</div>
				<textarea class="cart-req_text" ng-model="reqForm.values.text"></textarea>

				<div class="cart-req_sendBtn" ng-click="reqForm.onSendClick()">Отправить</div>

				<div class="cart-req_emptyCartWarning" ng-class="{'cart-req_emptyCartWarning__visible':reqForm.emptyCartWarning}">Ваша корзина пуста</div>

				<div class="cart-req_process-send" ng-class="{'cart-req_process-send__visible':reqForm.sendProcessing}">
					<img class="cart-req_process-send-img" src="/catalog/img/loading.gif">
				</div>
			</div>
		</div>

		<div class="cart-success" id="cart-success">
			<div class="cart-success_title">Ваша заявка отправлена!</div>
			<div class="cart-success_text">В ближайшее время с Вами свяжется наш менеджер.</div>
			<a class="cart-success_backBtn" href="/catalog">Вернуться в каталог</a>
		</div>
	</div>
</div>

<script type="text/javascript">
	var cartProducts = <?php echo CJavaScript::encode($cartProducts)?>;
	console.log(cartProducts);
</script>
<!DOCTYPE html>
<html lang="ru" >
<head>
	<meta content="text/html; charset=UTF-8" http-equiv="Content-Type" />	
	<title>Каталог</title>
	<meta name="viewport" content="width=1280" >
	<link rel="stylesheet" type="text/css" href="/catalog/css/resetExceptTable.css">
	<link rel="stylesheet" type="text/css" href="/catalog/css/layout.css?5">	
	<link rel="stylesheet" type="text/css" href="/catalog/css/style.css?5">	
	
</head>
<body>

<!-- <div class="header">
	<img class="header-demo" src="/catalog/img/header-demo.png">
</div> -->

<div id="header">
    <div class="header-firstline">
        <div class="logos-box">
            <span class="logos-both">
        <a href="/"><img src="/images/Logo_Vigma.png" alt="Логотип Вигма"></a>
      </span>
            <span class="logos-both">
        Официальный Дилер завода ЧТЗ-Уралтрак   
      </span>
        </div>
        <div id="contacts">
            <span class="cmmail"><a href="/feedback/">info@vigma.ru</a></span>
            <span class="cmphon"><span name="top" class="cphon_pref">(351)</span>215-16-66</span>
            <a href="#" data-reveal-id="myModal" class="callback">заказать звонок</a>
               
        </div>
        <nav id="nav-wrap">
            <div id="menu">
                <ul id="nav">
                    <li><a href="/news/">Новости</a></li>
                    <li><a href="/spectehnika/">Спецтехника</a></li>
                    <li><a href="/zapchasti/">Запчасти</a></li>
                    <li><a href="/stati/">Статьи</a></li>
                    <li><a href="/sklady/">Склады</a></li>
                    <li><a href="/kontakty/">Контакты</a></li>
                </ul>
            </div>
        </nav>
    </div>
    <div id="main-img">
        <img src="/images/photo_shapka3.png" alt="Бульдозеры в Челябинске на заводе ЧТЗ-Уралтрак">
    </div>
</div>


<div id="path">
    <a href="/">О компании</a>
    <span class="path_arrow">→</span>
    <a href="/catalog/">Каталог</a>
</div>


<div class="content" id="content">

    <?php if (!Yii::app()->user->isGuest): ?>
        <?php $url = $this->createUrl("login/logout") ?>
        <div class="adminpanel">
            <div class="ap_title">Вы вошли как администратор</div>
            <a class="ap_logout" href="<?php echo $url ?>">Выход</a>
        </div>
    <?php endif; ?>
	<?php echo $content; ?>	
</div>

<div class="update-browser" id="update-browser">
    <div class="update-browser_block">
        <div class="update-browser_title">Ваш браузер устраел</div>
         <div class="update-browser_info">Для корректной работы с каталогом установите один из следующих современных браузеров:</div>
         <div class="update-browser_items">
             <a class="update-browser_item" href="https://www.google.com/chrome/" target="_blank">
                 <div class="update-browser_item-img update-browser_item-img__chrome"></div>
                 <div class="update-browser_item-title">Chrome</div>
             </a>
             <a class="update-browser_item" href="https://www.mozilla.org/ru/" target="_blank">
                 <div class="update-browser_item-img update-browser_item-img__firefox"></div>
                 <div class="update-browser_item-title">Firefox</div>
             </a>
             <a class="update-browser_item" href="http://www.opera.com/ru" target="_blank">
                 <div class="update-browser_item-img update-browser_item-img__opera" ></div>
                 <div class="update-browser_item-title">Opera</div>
             </a>
         </div>
    </div>
</div>

<div id="footer">
    <div id="counters">
        
        <!--LiveInternet counter-->
        <script type="text/javascript">
            <!--
            document.write("<a href='http://www.liveinternet.ru/click' " +
                    "target=_blank><img src='http://counter.yadro.ru/hit?t26.11;r" +
                    escape(document.referrer) + ((typeof (screen) == "undefined") ? "" :
                        ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ?
                            screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) +
                    ";" + Math.random() +
                    "' alt='' title='LiveInternet: показано число посетителей за" +
                    " сегодня' " +
                    "border=0 width=88 height=15><\/a>") //-->
        </script>
        <!--/LiveInternet-->
    </div>
    <div id="tripple">
        <div class="up-od">
            <div id="up">
                <a href="#top"><img src="/images/up.png" alt="На верх"></a>
            </div>
            <div id="od-chtz">
                <p>Официальный Дилер
                    <br>ЧТЗ-Уралтрак
                    <br>техника и запчасти</p>
            </div>
        </div>
        <div id="foot-menu">
            <div id="foot-menu-col2">
                <ul>
                    <li class="main-foot-menu"><a href="http://vigma.ru/zapchasti/">Запчасти для бульдозеров</a></li>
                    <li class="main-foot-menu"><a href="http://vigma.ru/katalogi_i_instrukcii/">Каталоги запчастей</a></li>
                    <li class="sub-foot-menu"><a href="http://vigma.ru/pechatniye_katalogi/Traktory_T10M_Katalog_detaley_i_sborochnyh_edinic/Group_01/Block_dizelya/">Каталог на Б-10</a></li>
                    <li class="sub-foot-menu"><a href="http://vigma.ru/pechatniye_katalogi/tractor_t-170_m/soderzanie/poryadok_POlzovania/">Каталог на Т-170</a></li>
                    <li class="sub-foot-menu"><a href="http://vigma.ru/pechatniye_katalogi/T130Katalog_detaley_i_sborochnyh_edinic/Group_01/Block_dizelya/">Каталог на Т-130</a></li>
                    <li class="main-foot-menu"><a href="http://vigma.ru/spectehnika/truboukladchiki/">Трубоукладчики</a></li>
                    <li class="main-foot-menu"><a href="http://vigma.ru/spectehnika/buldozery_ryhlitely/">Рыхлители</a></li>
                </ul>
            </div>
            <div id="foot-menu-col1">
                <ul>
                    <li class="main-foot-menu"><a href="http://vigma.ru/spectehnika/buldozery/">Бульдозеры</a></li>
                    <li class="sub-foot-menu"><a href="http://vigma.ru/buldozer_b118000/">Бульдозер Б11.8000</a></li>
                    <li class="sub-foot-menu"><a href="http://vigma.ru/spectehnika/buldozery/buldozer_b10m/">Бульдозер Б-10</a></li>
                    <li class="sub-foot-menu"><a href="http://vigma.ru/spectehnika/buldozery/buldozer_b170/">Бульдозер Т-170</a></li>
                    <li class="sub-foot-menu"><a href="http://vigma.ru/spectehnika/buldozery/buldozer_T130/">Бульдозер Т-130</a></li>
                    <li class="main-foot-menu"><a href="http://vigma.ru/spectehnika/traktory/">Гусеничные тракторы</a></li>
                    <li class="sub-foot-menu"><a href="http://vigma.ru/spectehnika/traktory/traktor_t10m/">Трактор Б-10</a></li>
                    <li class="sub-foot-menu"><a href="http://vigma.ru/spectehnika/traktory/traktor_t170/">Трактор Т-170</a></li>
                    <li class="sub-foot-menu"><a href="http://vigma.ru/spectehnika/traktory/traktor_t130/">Трактор Т-130</a></li>
                </ul>
            </div>
        </div>

        <form id="foot-get" method="post" action="/feedback/footer/">
            <p>
                <input class="foot-get-name" type="text" name="name" placeholder=" Ваше имя" size="25" maxlength="40">
            </p>
            <p>
                <input class="foot-get-phone" type="text" name="phone" placeholder=" Ваш телефон или e-mail" size="25" maxlength="40">
            </p>
            <p>
                <textarea class="foot-get-msg" rows="6" cols="24" name="msg" placeholder=" Ваше сообщение"></textarea>
            </p>
            <p>
                <input class="foot-get-button" type="submit" />
            </p>
        </form>
        <div class="hod-success-bg" style="display:none;position:fixed;top:0;left:0;width:100%;height:100%;background-color:#333;opacity:0.9;"></div>
        <div class="hod-success" style="display:none;position:fixed;top:100px;left:50%;width:300px;margin-left:-150px;padding:20px;text-align:center;background-color:#fff;border:1px solid #ccc;border-radius:5px;">
            <p style="font-size:20px;"><b>Сообщение отправлено!</b></p>
            <button class="hod-success_hide">Ок</button>
        </div>

        <script type="text/javascript">
            $('.foot-get-button').click(function () {

                var name = $('.foot-get-name').val();
                var phone = $('.foot-get-phone').val();
                var msg = $('.foot-get-msg').val();

                if (phone == "") {
                    alert("Введите E-mail или телефон");
                    return false;
                }

                console.log(name);
                console.log(phone);
                console.log(msg);

                $.post(
                    "/feedback/footer/", {
                        name: name,
                        phone: phone,
                        msg: msg,
                    },
                    function (data) {
                        if (data == "true") {
                            $('.hod-success').show();
                            $('.hod-success-bg').show();
                            $('.foot-get-name').val('');
                            $('.foot-get-phone').val('');

                            $('.foot-get-msg').val('');
                        } else {
                            alert("Приносим свои извенения,на сайте произошла ошибка. Позвоните по номеру указанному в контактах.");
                        }

                    }
                );


                return false;
            });;

            $('.hod-success_hide').click(function () {
                $('.hod-success').hide();
                $('.hod-success-bg').hide();
            });
        </script>
    </div>
    <div id="foot-kompany">
        <div class="foot-kompany-logo-name">
            <a href="/"><img src="/images/logo_footer.png" alt="Логотоип Вигма"><span class="foot-kompany-name">ВИГМА</span></a>
        </div>
        <span class="foot-kompany-address">454091, г. Челябинск, пр. Ленина, 23-10</span><span class="foot-kompany-phone">(351) 215-16-66</span>
    </div>

</div>

<script> 
var $buoop = {vs:{i:10,f:40,o:30,s:8,c:30},api:4,reminder:0,mobile:false,nomessage: true,onshow: function(infos) {
    var content = document.getElementById("content");
    content.className += " content__hide";
    var updateBlock = document.getElementById('update-browser');
    updateBlock.className += " update-browser__visible";
}}; 
function $buo_f(){ 
 var e = document.createElement("script"); 
 e.src = "//browser-update.org/update.min.js"; 
 document.body.appendChild(e);
};

// function showOldBrowserDetect(infos)
// {
//     alert(infos);
// }
try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
catch(e){window.attachEvent("onload", $buo_f)}
</script>
		
</body>
</html>


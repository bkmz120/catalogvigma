-- phpMyAdmin SQL Dump
-- version 4.0.10.10
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 06 2016 г., 09:25
-- Версия сервера: 10.0.21-MariaDB
-- Версия PHP: 5.5.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `galina111_vi`
--

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `original_name` varchar(256) DEFAULT NULL,
  `original_chod` varchar(256) DEFAULT NULL,
  `catalog_name` varchar(255) DEFAULT NULL,
  `catalog_chod` varchar(255) DEFAULT NULL,
  `chod_display` varchar(255) DEFAULT NULL COMMENT 'ЧОД как он есть (со всеми разделителями)',
  `product_num` varchar(256) DEFAULT NULL,
  `comment` text,
  `tovar_type` varchar(20) DEFAULT NULL,
  `other_info` text,
  `defaultprovider_id` int(11) DEFAULT '1',
  `defaultprovprod_id` int(11) DEFAULT NULL COMMENT 'связь с поставщиком, котрый будет выбран по-умолчанию при добавлении товара в заявку. Это замена defaultprovider_id',
  `price_chtz` double DEFAULT NULL,
  `price_catalog` decimal(12,2) DEFAULT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `price_out` float DEFAULT NULL,
  `size` varchar(256) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `parts_id` int(11) DEFAULT NULL,
  `partof_id` int(11) DEFAULT NULL,
  `use_deafult` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=19146 ;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `original_name`, `original_chod`, `catalog_name`, `catalog_chod`, `chod_display`, `product_num`, `comment`, `tovar_type`, `other_info`, `defaultprovider_id`, `defaultprovprod_id`, `price_chtz`, `price_catalog`, `hidden`, `price_out`, `size`, `weight`, `img`, `parts_id`, `partof_id`, `use_deafult`) VALUES
(12654, 'КОЛЬЦО 010-014-25-2-2', '0100142522', 'КОЛЬЦО', '010-014-25-2-2', '010-014-25-2-2', '9044', NULL, NULL, NULL, 1, 1, 8.15, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL),
(12655, 'Диффузор', '011047', 'Диффузор', '011047', '011047', '9044', NULL, NULL, NULL, 1, 2, 19.25, NULL, 0, NULL, NULL, 0.058, NULL, NULL, NULL, NULL),
(12656, 'Заглушка', '01173', 'Заглушка', '01173', '01173', '9044', NULL, NULL, NULL, 1, 3, 17.85, NULL, 0, NULL, NULL, 0.115, NULL, NULL, NULL, NULL),
(12657, 'Крышка люка', '01187', 'Крышка люка', '01187', '01187', '7001', NULL, NULL, NULL, 1, 4, 97.8, NULL, 0, NULL, NULL, 0.27, NULL, NULL, NULL, NULL),
(12658, 'Заглушка', '01196', 'Заглушка', '01196', '01196', '9044', NULL, NULL, NULL, 1, 5, 9.25, NULL, 0, NULL, NULL, 0.031, NULL, NULL, NULL, NULL),
(12659, 'Крышка сапуна', '01200', 'Крышка сапуна', '01200', '01200', '9044', NULL, NULL, NULL, 1, 6, 6.9, NULL, 0, NULL, NULL, 0.027, NULL, NULL, NULL, NULL),
(12660, 'Дистанционная шайба', '01207', 'Дистанционная шайба', '01207', '01207', '9044', NULL, NULL, NULL, 1, 7, 0.25, NULL, 0, NULL, NULL, 0.0005, NULL, NULL, NULL, NULL),
(12661, 'Сапун', '01265СП', 'Сапун', '01265СП', '01265СП', '7001', NULL, NULL, NULL, 1, 8, 349.8, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL),
(12662, 'Заглушка', '013', 'Заглушка', '013', '013', '9044', NULL, NULL, NULL, 1, 9, 3.85, NULL, 0, NULL, NULL, 0.021, NULL, NULL, NULL, NULL),
(12663, 'КОЛЬЦО', '0140182522', 'КОЛЬЦО', '014-018-25-2-2', '014-018-25-2-2', '9044', NULL, NULL, NULL, 1, 10, 8.4, NULL, 0, NULL, NULL, 0.0003, NULL, NULL, NULL, NULL),
(12664, 'КОЛЬЦО 014-018-25-2-5', '0140182525', 'КОЛЬЦО', '014-018-25-2-5', '014-018-25-2-5', '9017', NULL, NULL, NULL, 1, 11, 11.9, NULL, 0, NULL, NULL, 0.0003, NULL, NULL, NULL, NULL),
(12665, 'Шпилька', '01403', 'Шпилька', '01403', '01403', '7001', NULL, NULL, NULL, 1, 12, 209.3, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL),
(12666, 'Шпилька', '01434', 'Шпилька', '01434', '01434', '7001', NULL, NULL, NULL, 1, 13, 97.8, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL),
(12667, 'Втулка', '01459', 'Втулка', '01459', '01459', '7001', NULL, NULL, NULL, 1, 14, 633.4, NULL, 0, NULL, NULL, 0.317, NULL, NULL, NULL, NULL),
(12668, 'Ось шестерни', '01463', 'Ось шестерни', '01463', '01463', '7001', NULL, NULL, NULL, 1, 15, 1370.2, NULL, 0, NULL, NULL, 1.213, NULL, NULL, NULL, NULL),
(12669, 'Трубка', '01464', 'Трубка', '01464', '01464', '9044', NULL, NULL, NULL, 1, 16, 5.65, NULL, 0, NULL, NULL, 0.002, NULL, NULL, NULL, NULL),
(12670, 'ГИЛЬЗА 01466-2', '014662', 'ГИЛЬЗА', '01466-2', '01466-2', '9017', 'Д 145 КМЗ (Камский моторный завод).  Для Д150: 51-02-82\nНужно брать КМЗ, не Костромской. Косторома специализируется на сельхоз технике, для ЧТЗ нугрузки выше.\nНа КМЗ более современное оборудование, лучше геометрия, более строгий контроль качества, срок службы дольше.\n\nГильза из специального легированного чугуна. Особенность материала - плотная структура, обеспечивающая долговечность гильзы. Метод изготовления - центробежное литье. Обработка: плосковершинное хонингование\nГильзы к двигателю Д-160,Д-180 Камского моторного завода производятся по технологии “каленые” гильзы. В случаях, когда к гильзам  предъявляются более высокие требования в отношении износо- и коррозионной стойкости, гильзы Д 160 Д 180 могут быть подвержены фосфатированию (преимущества фосфатирования?)\nСсылка на сайт КМЗ: http://www.porshen.ru/Porshnevye-gruppy-traktorov-cena/gilzy-chtz/gilzy-chtz_945.html', NULL, NULL, 1, 17, 1289.4, NULL, 0, NULL, '38,2;0;0', 14.5, NULL, NULL, NULL, NULL),
(12671, 'Крышка люка', '01467', 'Крышка люка', '01467', '01467', '7001', NULL, NULL, NULL, 1, 18, 321.2, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL),
(12672, 'Крышка', '01479', 'Крышка', '01479', '01479', '9044', NULL, NULL, NULL, 1, 19, 23.2, NULL, 0, NULL, NULL, 0.342, NULL, NULL, NULL, NULL),
(12673, 'Прокладка', '01481', 'Прокладка', '01481', '01481', '9044', NULL, NULL, NULL, 1, 20, 4.65, NULL, 0, NULL, NULL, 0.034, NULL, NULL, NULL, NULL),
(12674, 'Прокладка регулирования', '01482', 'Прокладка регулирования', '01482', '01482', '9044', NULL, NULL, NULL, 1, 21, 1.55, NULL, 0, NULL, NULL, 0.014, NULL, NULL, NULL, NULL),
(12675, 'Пробка', '01483СП', 'Пробка', '01483СП', '01483СП', '7001', NULL, NULL, NULL, 1, 22, 223.7, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL),
(12676, 'Пробка', '01484СП', 'Пробка', '01484СП', '01484СП', '7001', NULL, NULL, NULL, 1, 23, 168.2, NULL, 0, NULL, NULL, 0.45, NULL, NULL, NULL, NULL),
(12677, 'КОЛЬЦО 015-019-25-2-2', '0150192522', 'КОЛЬЦО', '015-019-25-2-2', '015-019-25-2-2', '9017', NULL, NULL, NULL, 1, 24, 3.45, NULL, 0, NULL, NULL, 0.00029, NULL, NULL, NULL, NULL),
(12678, 'Втулка клапана', '01504', 'Втулка клапана', '01504', '01504', '7001', NULL, NULL, NULL, 1, 25, 140.3, NULL, 0, NULL, NULL, 0.08, NULL, NULL, NULL, NULL),
(12679, 'Крышка заднего люка блока', '01510', 'Крышка заднего люка блока', '01510', '01510', '7001', NULL, NULL, NULL, 1, 26, 84.6, NULL, 0, NULL, NULL, 0.39, NULL, NULL, NULL, NULL),
(12680, 'КОЛЬЦО 016-020-25-2-2', '0160202522', 'КОЛЬЦО', '016-020-25-2-2', '016-020-25-2-2', '9044', NULL, NULL, NULL, 1, 27, 2.85, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL),
(12681, 'КОЛЬЦО 016-021-30-2-2', '0160213022', 'КОЛЬЦО', '016-021-30-2-2', '016-021-30-2-2', '9017', NULL, NULL, NULL, 1, 28, 2.45, NULL, 0, NULL, NULL, 0.001, NULL, NULL, NULL, NULL),
(12682, 'КОЛЬЦО 018-022-25-2-2', '0180222522', 'КОЛЬЦО', '018-022-25-2-2', '018-022-25-2-2', '9044', NULL, NULL, NULL, 1, 29, 10.25, NULL, 0, NULL, NULL, 0.0003, NULL, NULL, NULL, NULL),
(12683, 'КОЛЬЦО 018-022-25-2-5', '0180222525', 'КОЛЬЦО', '018-022-25-2-5', '018-022-25-2-5', '9017', NULL, NULL, NULL, 6, 7499, 10.05, NULL, 0, NULL, NULL, 0.0003, NULL, NULL, NULL, NULL),
(12684, 'КОЛЬЦО 020-025-30-2-2', '0200253022', 'КОЛЬЦО', '020-025-30-2-2', '020-025-30-2-2', '9044', NULL, NULL, NULL, 1, 31, 11.75, NULL, 0, NULL, NULL, 0.0005, NULL, NULL, NULL, NULL),
(12685, 'КОЛЬЦО 020-025-30-2-5', '0200253025', 'КОЛЬЦО', '020-025-30-2-5', '020-025-30-2-5', '9017', NULL, NULL, NULL, 6, 7496, 10.05, NULL, 0, NULL, NULL, 0.0005, NULL, NULL, NULL, NULL),
(12686, 'ВЕНТИЛЯТОР НАКРЫШНЫЙ 02-0300', '020300', 'ВЕНТИЛЯТОР НАКРЫШНЫЙ', '02-0300', '02-0300', '9017', NULL, NULL, NULL, 1, 33, 2572.8, NULL, 0, NULL, NULL, 1.3, NULL, NULL, NULL, NULL),
(12687, 'Кольцо', '0220283622', 'Кольцо', '022-028-36-2-2', '022-028-36-2-2', '9044', NULL, NULL, NULL, 6, 7500, 1.65, NULL, 0, NULL, NULL, 0.0008, NULL, NULL, NULL, NULL),
(12688, 'КОЛЬЦО 022-028-36-2-5', '0220283625', 'КОЛЬЦО', '022-028-36-2-5', '022-028-36-2-5', '9017', NULL, NULL, NULL, 1, 35, 12.95, NULL, 0, NULL, NULL, 2, NULL, NULL, NULL, NULL),
(12689, 'Штанга декомпрессора', '02219', 'Штанга декомпрессора', '02219', '02219', '7001', NULL, NULL, NULL, 1, 36, 80.2, NULL, 0, NULL, NULL, 0.145, NULL, NULL, NULL, NULL),
(12690, 'Палец вилки тяги декомпрессора', '02226', 'Палец вилки тяги декомпрессора', '02226', '02226', '7001', NULL, NULL, NULL, 1, 37, 16.3, NULL, 0, NULL, NULL, 0.012, NULL, NULL, NULL, NULL),
(12691, 'Подшипник валика декомпрессора', '02236', 'Подшипник валика декомпрессора', '02236', '02236', '7001', NULL, NULL, NULL, 1, 38, 140.3, NULL, 0, NULL, NULL, 0.3, NULL, NULL, NULL, NULL),
(12692, 'Головка цилиндров', '0224001', 'Головка цилиндров', '02240-01', '02240-01', '7001', NULL, NULL, NULL, 1, 39, 1296.1, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL),
(12693, 'Наконечник', '02277', 'Наконечник', '02277', '02277', '7001', NULL, NULL, NULL, 1, 40, 48.5, NULL, 0, NULL, NULL, 0.022, NULL, NULL, NULL, NULL),
(12694, 'КОЛЬЦО 024-030-36-2-2', '0240303622', 'КОЛЬЦО', '024-030-36-2-2', '024-030-36-2-2', '9044', NULL, NULL, NULL, 1, 41, 4.25, NULL, 0, NULL, NULL, 0, NULL, NULL, NULL, NULL),
(12695, 'Штуцер угловой', '0270404030', 'Штуцер угловой', '027.04.04.030', '027.04.04.030', '7005', NULL, NULL, NULL, 1, 42, 2323, NULL, 0, NULL, NULL, 0.65, NULL, NULL, NULL, NULL),
(12696, 'Штуцер', '0270602051', 'Штуцер', '027.06.02.051', '027.06.02.051', '9017', NULL, NULL, NULL, 1, 43, 154.55, NULL, 0, NULL, NULL, 0.03, NULL, NULL, NULL, NULL),
(12697, 'Болт карданный', '0271150009', 'Болт карданный', '027.11.50.009', '027.11.50.009', '9017', '', NULL, NULL, 1, 44, 101.7, NULL, 0, NULL, NULL, 0.023, '/photos/1471952627.jpg', NULL, NULL, NULL),
(12698, 'Болт карданный', '0271150012', 'Болт карданный', '027.11.50.012', '027.11.50.012', '7005', NULL, NULL, NULL, 1, 45, 703.4, NULL, 0, NULL, NULL, 0.051, NULL, NULL, NULL, NULL),
(12699, 'Гайка', '0271311076', 'Гайка', '027.13.11.076', '027.13.11.076', '7005', NULL, NULL, NULL, 1, 46, 259.1, NULL, 0, NULL, NULL, 0.05, NULL, NULL, NULL, NULL),
(12700, 'Гайка', '0271312227', 'Гайка', '027.13.12.227', '027.13.12.227', '7005', NULL, NULL, NULL, 1, 47, 254.5, NULL, 0, NULL, NULL, 0.09, NULL, NULL, NULL, NULL),
(12701, 'Гайка', '0271314227', 'Гайка', '027.13.14.227', '027.13.14.227', '7005', NULL, NULL, NULL, 1, 48, 262.2, NULL, 0, NULL, NULL, 0.28, NULL, NULL, NULL, NULL),
(12702, 'Гайка', '0271342442', 'Гайка', '027.13.42.442', '027.13.42.442', '9017', NULL, NULL, NULL, 1, 49, 190.85, NULL, 0, NULL, NULL, 0.38, NULL, NULL, NULL, NULL),
(12703, 'Шайба', '0271411035', 'Шайба', '027.14.11.035', '027.14.11.035', '7005', NULL, NULL, NULL, 1, 50, 69.4, NULL, 0, NULL, NULL, 0.13, NULL, NULL, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
